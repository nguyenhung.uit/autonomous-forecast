#include <jni.h>
#include <string>

extern "C" JNIEXPORT jstring
JNICALL
Java_uit_nguyenhung_autonomousforecast_Keys_getOpenCageKey(JNIEnv *env) {
    std::string api_key = "64b4adc491c8476e93772284769a4005";
    return env->NewStringUTF(api_key.c_str());
}

extern "C" JNIEXPORT jstring
JNICALL
Java_uit_nguyenhung_autonomousforecast_Keys_getOpenWeatherAppId(JNIEnv *env) {
    std::string app_id = "2ed34880c948e7e9434e5f951f96e18b";
    return env->NewStringUTF(app_id.c_str());
}