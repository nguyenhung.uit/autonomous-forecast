package uit.nguyenhung.autonomousforecast.app.feature.cage

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import dagger.hilt.android.AndroidEntryPoint
import uit.nguyenhung.autonomousforecast.R
import uit.nguyenhung.autonomousforecast.app.model.WeatherUI
import uit.nguyenhung.autonomousforecast.app.network.NetworkObservable
import uit.nguyenhung.autonomousforecast.app.network.NetworkObserver
import uit.nguyenhung.autonomousforecast.databinding.ActivityCageBinding
import uit.nguyenhung.autonomousforecast.shared.hideKeyboard
import uit.nguyenhung.autonomousforecast.shared.visibleOrGone

@AndroidEntryPoint
class CageActivity : AppCompatActivity() {

    companion object {

        const val CAGE_ACTIVITY_REQUEST_CODE = 90

        const val WEATHERS_KEY = "WEATHERS_KEY"

        const val CITY_NAME_KEY = "CITY_NAME_KEY"

        fun startActivityForResult(context: Activity, cityName: CharSequence) {
            context.startActivityForResult(
                Intent(context, CageActivity::class.java)
                    .apply {
                        putExtra(CITY_NAME_KEY, cityName)
                    },
                CAGE_ACTIVITY_REQUEST_CODE
            )
        }
    }

    private lateinit var viewModel: ICageViewModel

    private lateinit var adapter: CageAdapter

    private val binding: ActivityCageBinding by lazy {
        DataBindingUtil.setContentView(this, R.layout.activity_cage)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProvider(
            this,
            defaultViewModelProviderFactory
        ).get(CageViewModel::class.java)

        NetworkObservable.getInstance(this)
            .subscribe(NetworkObserver(this@CageActivity) { isAvailable ->
                val msg = "internet is " + if (isAvailable) "on" else "off"
                Toast.makeText(this@CageActivity, msg, Toast.LENGTH_SHORT)
                    .show()

                if (isAvailable) {
                    viewModel.checkPendingSearch()
                }
            })

        binding.initView()
        viewModel.initViewModel()
    }

    private fun ActivityCageBinding.initView() {
        adapter = CageAdapter(rvCage) {
            edtSearch.hideKeyboard()
            edtSearch.setText(it)
            viewModel.getWeatherByCityName(it)
        }

        intent.getCharSequenceExtra(CITY_NAME_KEY)?.let {
            edtSearch.setText(it)
        }

        edtSearch.requestFocus()

        btnSearch.setOnClickListener {
            edtSearch.hideKeyboard()
            viewModel.getWeatherByCityName(edtSearch.text.toString())
        }
    }

    private fun ICageViewModel.initViewModel() {
        getCityNameHistory()

        cageHistoryUI.observe(this@CageActivity, {
            adapter.submitList(it)
        })

        weatherUI.observe(this@CageActivity, {
            handleNavigateSearchResult(it)
        })

        loading.observe(this@CageActivity, {
            it.getContentIfNotHandled()?.let { isShow ->
                binding.pbSearch.visibleOrGone(isShow)
            }
        })

        inputError.observe(this@CageActivity, {
            it.getContentIfNotHandled()?.let { msg ->
                binding.edtSearch.error = msg
            }
        })

        commonError.observe(this@CageActivity, {
            it.getContentIfNotHandled()?.let { msg ->
                Toast.makeText(this@CageActivity, msg, Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun handleNavigateSearchResult(weathers: List<WeatherUI>) {
        setResult(RESULT_OK, Intent().apply {
            putExtras(
                Bundle().apply {
                    putParcelableArrayList(WEATHERS_KEY, ArrayList<Parcelable>(weathers))
                    putString(CITY_NAME_KEY, binding.edtSearch.text.toString())
                }
            )
        })
        finish()
    }
}