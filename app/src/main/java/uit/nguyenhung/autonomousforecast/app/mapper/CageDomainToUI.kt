package uit.nguyenhung.autonomousforecast.app.mapper

import uit.nguyenhung.autonomousforecast.app.model.CageUI
import uit.nguyenhung.autonomousforecast.domain.model.Cage
import uit.nguyenhung.autonomousforecast.shared.ModelMapper
import javax.inject.Inject

class CageDomainToUI @Inject constructor() : ModelMapper<Cage, CageUI> {

    override fun map(from: Cage) = CageUI(
        cityName = from.cityName
    )
}