package uit.nguyenhung.autonomousforecast.app

import android.content.Context
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import androidx.recyclerview.widget.RecyclerView

class DividerItemDecoration(context: Context) : RecyclerView.ItemDecoration() {

    private var drawableDivider: Drawable? = null

    init {
        val styledAttributes = context.obtainStyledAttributes(ATTRS)
        drawableDivider = styledAttributes.getDrawable(0)
        styledAttributes.recycle()
    }

    override fun onDraw(canvas: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        val left = parent.paddingLeft
        val right = parent.width - parent.paddingRight

        val childCount = parent.childCount

        for (i in 0 until childCount) {
            val child = parent.getChildAt(i)
            val params = child.layoutParams as RecyclerView.LayoutParams
            val top = child.bottom + params.bottomMargin
            val bottom = top + (drawableDivider?.intrinsicHeight ?: 0)

            drawableDivider?.setBounds(left, top, right, bottom)
            drawableDivider?.draw(canvas)
        }
    }

    companion object {

        private val ATTRS = intArrayOf(android.R.attr.listDivider)
    }
}
