package uit.nguyenhung.autonomousforecast.app.feature.cage

import androidx.recyclerview.widget.DiffUtil
import uit.nguyenhung.autonomousforecast.app.model.CageUI

class CageDiffCallback(
    private val oldNormal: List<CageUI>,
    private val newNormal: List<CageUI>
) : DiffUtil.Callback() {

    override fun getOldListSize() = oldNormal.size

    override fun getNewListSize() = newNormal.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldItem = oldNormal[oldItemPosition]
        val newItem = newNormal[newItemPosition]
        return oldItem.cityName == newItem.cityName
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldItem = oldNormal[oldItemPosition]
        val newItem = newNormal[newItemPosition]
        return oldItem == newItem
    }
}