package uit.nguyenhung.autonomousforecast.app.feature.weather

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import uit.nguyenhung.autonomousforecast.R
import uit.nguyenhung.autonomousforecast.app.DividerItemDecoration
import uit.nguyenhung.autonomousforecast.app.model.WeatherUI
import uit.nguyenhung.autonomousforecast.databinding.ItemWeatherBinding

class WeatherAdapter(
    recyclerView: RecyclerView
) : RecyclerView.Adapter<WeatherAdapter.WeatherViewHolder>() {

    init {
        recyclerView.run {
            adapter = this@WeatherAdapter
            addItemDecoration(DividerItemDecoration(context))
        }
    }

    val data: MutableList<WeatherUI> = mutableListOf()

    private val mDisposablesOnDestroy: CompositeDisposable = CompositeDisposable()

    private fun Disposable.addToDisposables() {
        this.addTo(mDisposablesOnDestroy)
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        super.onDetachedFromRecyclerView(recyclerView)
        mDisposablesOnDestroy.dispose()
    }

    fun submitList(newList: List<WeatherUI>) {

        val diffCallback = WeatherDiffCallback(data, newList)
        Single.fromCallable { DiffUtil.calculateDiff(diffCallback) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                data.clear()
                data.addAll(newList)
                it.dispatchUpdatesTo(this)
            }, {})
            .addToDisposables()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WeatherViewHolder {
        return WeatherViewHolder(parent)
    }

    override fun onBindViewHolder(holder: WeatherViewHolder, position: Int) {
        holder.onBind(data[position])
    }

    override fun getItemCount() = data.size

    inner class WeatherViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(
        parent.inflateViewHolder(R.layout.item_weather)
    ) {

        private val viewBinding = ItemWeatherBinding.bind(itemView)

        @SuppressLint("SetTextI18n")
        fun onBind(item: WeatherUI) {
            viewBinding.run {
                tvDate.text = "Date: ${item.date}"
                tvAvgTemperature.text = "Temperature: ${item.temperature}"
                tvPressure.text = "Pressure: ${item.pressure}"
                tvHumidity.text = "Humidity: ${item.humidity}"
                tvDescription.text = "Description: ${item.description}"
            }
        }
    }

    fun ViewGroup.inflateViewHolder(id: Int): View {
        return LayoutInflater.from(context).inflate(id, this, false)
    }
}