package uit.nguyenhung.autonomousforecast.app.feature.weather

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import uit.nguyenhung.autonomousforecast.app.BaseViewModel
import uit.nguyenhung.autonomousforecast.app.model.WeatherUI

interface IWeatherViewModel {
    val weatherUI: LiveData<List<WeatherUI>>
    val cityName: LiveData<String>
    fun setWeatherUI(weathers: List<WeatherUI>)
    fun setCityName(cityName: String)
}

class WeatherViewModel @ViewModelInject constructor(

) : BaseViewModel(), IWeatherViewModel {

    private val _weatherUI = MutableLiveData<List<WeatherUI>>()
    override val weatherUI: LiveData<List<WeatherUI>>
        get() = _weatherUI

    private val _cityName = MutableLiveData<String>()
    override val cityName: LiveData<String>
        get() = _cityName

    override fun setWeatherUI(weathers: List<WeatherUI>) {
        _weatherUI.postValue(weathers)
    }

    override fun setCityName(cityName: String) {
        _cityName.postValue(cityName)
    }
}