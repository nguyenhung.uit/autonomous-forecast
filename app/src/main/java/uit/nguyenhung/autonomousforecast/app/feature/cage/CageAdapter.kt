package uit.nguyenhung.autonomousforecast.app.feature.cage

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import uit.nguyenhung.autonomousforecast.R
import uit.nguyenhung.autonomousforecast.app.DividerItemDecoration
import uit.nguyenhung.autonomousforecast.app.model.CageUI
import uit.nguyenhung.autonomousforecast.databinding.ItemCageBinding

class CageAdapter(
    recyclerView: RecyclerView,
    private val onItemClick: (String) -> Unit = {}
) : RecyclerView.Adapter<CageAdapter.CageViewHolder>() {
    init {
        recyclerView.run {
            adapter = this@CageAdapter
            addItemDecoration(DividerItemDecoration(context))
        }
    }

    val data: MutableList<CageUI> = mutableListOf()

    private val mDisposablesOnDestroy: CompositeDisposable = CompositeDisposable()

    private fun Disposable.addToDisposables() {
        this.addTo(mDisposablesOnDestroy)
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        super.onDetachedFromRecyclerView(recyclerView)
        mDisposablesOnDestroy.dispose()
    }

    fun submitList(newList: List<CageUI>) {
        val diffCallback = CageDiffCallback(data, newList)
        Single.fromCallable { DiffUtil.calculateDiff(diffCallback) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                data.clear()
                data.addAll(newList)
                it.dispatchUpdatesTo(this)
            }, {})
            .addToDisposables()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CageViewHolder {
        val holder = CageViewHolder(parent)
        holder.itemView.setOnClickListener {
            onItemClick(data[holder.adapterPosition].cityName)
        }
        return holder
    }

    override fun onBindViewHolder(holder: CageViewHolder, position: Int) {
        holder.onBind(data[position])
    }

    override fun getItemCount() = data.size

    inner class CageViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(
        parent.inflateViewHolder(R.layout.item_cage)
    ) {

        private val viewBinding = ItemCageBinding.bind(itemView)

        @SuppressLint("SetTextI18n")
        fun onBind(item: CageUI) {
            viewBinding.run {
                tvCityName.text = item.cityName
            }
        }
    }

    fun ViewGroup.inflateViewHolder(id: Int): View {
        return LayoutInflater.from(context).inflate(id, this, false)
    }
}