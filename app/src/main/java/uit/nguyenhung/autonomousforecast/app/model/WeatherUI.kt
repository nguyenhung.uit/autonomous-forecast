package uit.nguyenhung.autonomousforecast.app.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class WeatherUI(
    val id: Long?,
    val date: String?,
    val temperature: String?,
    val pressure: String?,
    val humidity: String?,
    val description: String?,
) : Parcelable