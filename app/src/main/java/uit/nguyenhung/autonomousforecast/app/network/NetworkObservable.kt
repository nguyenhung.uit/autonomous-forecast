package uit.nguyenhung.autonomousforecast.app.network

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkRequest
import android.os.Build
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.LifecycleOwner


interface INetworkObservable {
    fun getObservers(): HashMap<String, INetworkObserver>
    fun notifyNetworkChange(isAvailable: Boolean)
}

interface INetworkObserver {
    var observable: INetworkObservable?
    val key: String
    var onChange: (isAvailable: Boolean) -> Unit
}

class NetworkObservable(
    context: Context
) : INetworkObservable {


    companion object {

        @Volatile
        private var INSTANCE: NetworkObservable? = null

        @JvmStatic
        @Synchronized
        fun getInstance(context: Context): NetworkObservable {
            synchronized(NetworkObservable::class.java) {
                if (INSTANCE == null) {
                    INSTANCE = NetworkObservable(context)
                }
            }

            return INSTANCE!!
        }
    }

    init {
        val callback = object : ConnectivityManager.NetworkCallback() {
            override fun onAvailable(network: Network) {
                notifyNetworkChange(true)
            }

            override fun onLost(network: Network) {
                notifyNetworkChange(false)
            }
        }

        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            connectivityManager.registerDefaultNetworkCallback(callback)
        } else {
            val builder = NetworkRequest.Builder()
            connectivityManager.registerNetworkCallback(builder.build(), callback)
        }

        /*
        // Retrieve current status of connectivity
        connectivityManager.allNetworks.forEach { network ->
            val networkCapability = connectivityManager.getNetworkCapabilities(network)

            networkCapability?.let {
                if (it.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)) {
                    isAvailable = true
                    return@forEach
                }
            }
        }*/
    }

    private val observers = hashMapOf<String, INetworkObserver>()

    override fun getObservers(): HashMap<String, INetworkObserver> = observers

    override fun notifyNetworkChange(isAvailable: Boolean) {
        observers.forEach {
            it.value.onChange(isAvailable)
        }
    }

    fun subscribe(
        themeObserver: NetworkObserver,
    ): NetworkObservable {
        observers[themeObserver.key] = themeObserver.apply {
            observable = this@NetworkObservable
        }

        return this@NetworkObservable
    }
}

open class NetworkObserver(
    private val owner: LifecycleOwner,
    override var onChange: (isAlready: Boolean) -> Unit = {}
) : INetworkObserver {
    init {
        owner.lifecycle.addObserver(object : LifecycleEventObserver {
            override fun onStateChanged(source: LifecycleOwner, event: Lifecycle.Event) {
                when (event) {
                    Lifecycle.Event.ON_DESTROY -> {
                        owner.lifecycle.removeObserver(this)
                        unSubscribe(this@NetworkObserver)
                    }
                    Lifecycle.Event.ON_START -> {
                        //observable?.notifyNetworkChange()
                    }
                    else -> {

                    }
                }
            }
        })
    }

    override var observable: INetworkObservable? = null

    override val key: String
        get() = owner.javaClass.name

    fun unSubscribe(observer: INetworkObserver) {
        observable?.getObservers()?.remove(observer.key)
    }
}