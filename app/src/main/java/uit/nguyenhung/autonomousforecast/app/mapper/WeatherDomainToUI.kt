package uit.nguyenhung.autonomousforecast.app.mapper

import uit.nguyenhung.autonomousforecast.app.model.WeatherUI
import uit.nguyenhung.autonomousforecast.domain.model.Weather
import uit.nguyenhung.autonomousforecast.shared.ModelMapper
import uit.nguyenhung.autonomousforecast.shared.format
import uit.nguyenhung.autonomousforecast.shared.safe
import javax.inject.Inject

class WeatherDomainToUI @Inject constructor() : ModelMapper<Weather, WeatherUI> {

    override fun map(from: Weather) = WeatherUI(
        id = from.date?.time,
        date = from.date?.format(),
        temperature = "${from.temperature.safe()}",
        pressure = "${from.pressure}",
        humidity = "${from.humidity}%",
        description = from.description,
    )
}