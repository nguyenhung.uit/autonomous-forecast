package uit.nguyenhung.autonomousforecast.app

import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.addTo

open class BaseViewModel : ViewModel() {

    private val mDisposablesOnDestroy: CompositeDisposable = CompositeDisposable()

    fun Disposable.addToDisposables() {
        this.addTo(mDisposablesOnDestroy)
    }

    override fun onCleared() {
        super.onCleared()
        mDisposablesOnDestroy.dispose()
    }
}