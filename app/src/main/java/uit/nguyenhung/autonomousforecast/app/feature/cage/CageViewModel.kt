package uit.nguyenhung.autonomousforecast.app.feature.cage

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import uit.nguyenhung.autonomousforecast.app.BaseViewModel
import uit.nguyenhung.autonomousforecast.app.Event
import uit.nguyenhung.autonomousforecast.app.mapper.CageDomainToUI
import uit.nguyenhung.autonomousforecast.app.mapper.WeatherDomainToUI
import uit.nguyenhung.autonomousforecast.app.model.CageUI
import uit.nguyenhung.autonomousforecast.app.model.WeatherUI
import uit.nguyenhung.autonomousforecast.domain.usecase.GetCityNameHistoryUseCase
import uit.nguyenhung.autonomousforecast.domain.usecase.GetWeatherByCityNameUseCase
import uit.nguyenhung.autonomousforecast.shared.exception.InputEmptyException
import uit.nguyenhung.autonomousforecast.shared.exception.NoConnectionException
import uit.nguyenhung.autonomousforecast.shared.safe

interface ICageViewModel {
    val cageHistoryUI: LiveData<List<CageUI>>
    val weatherUI: LiveData<List<WeatherUI>>
    val loading: LiveData<Event<Boolean>>
    val commonError: LiveData<Event<String>>
    val inputError: LiveData<Event<String>>
    fun checkPendingSearch()
    fun getWeatherByCityName(cityNameQuery: String)
    fun getCityNameHistory()
}


class CageViewModel @ViewModelInject constructor(
    private val getWeatherByCityNameUseCase: GetWeatherByCityNameUseCase,
    private val getCityNameHistoryUseCase: GetCityNameHistoryUseCase,
    private val cageDomainToUI: CageDomainToUI,
    private val weatherDomainToUI: WeatherDomainToUI,
) : BaseViewModel(), ICageViewModel {

    private val _cageHistoryUI = MutableLiveData<List<CageUI>>()
    override val cageHistoryUI: LiveData<List<CageUI>>
        get() = _cageHistoryUI

    private val _weatherUI = MutableLiveData<List<WeatherUI>>()
    override val weatherUI: LiveData<List<WeatherUI>>
        get() = _weatherUI

    private val _loading = MutableLiveData<Event<Boolean>>()
    override val loading: LiveData<Event<Boolean>>
        get() = _loading

    private val _commonError = MutableLiveData<Event<String>>()
    override val commonError: LiveData<Event<String>>
        get() = _commonError

    private val _inputError = MutableLiveData<Event<String>>()
    override val inputError: LiveData<Event<String>>
        get() = _inputError

    private var pendingSearchKeyWord: String? = null

    override fun checkPendingSearch() {
        if (pendingSearchKeyWord.isNullOrEmpty()) return
        getWeatherByCityName(pendingSearchKeyWord.safe())
    }

    override fun getWeatherByCityName(cityNameQuery: String) {
        getWeatherByCityNameUseCase.execute(cityNameQuery)
            .map {
                it.map { weather ->
                    weatherDomainToUI.map(weather)
                }
            }
            .doOnSuccess {
                pendingSearchKeyWord = null
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                _loading.postValue(Event(true))
            }
            .doFinally {
                _loading.postValue(Event(false))
            }
            .subscribe({
                _weatherUI.value = it
            }, {
                when (it) {
                    is InputEmptyException -> {
                        _inputError.value = Event(it.message.safe())
                    }

                    is NoConnectionException -> {
                        pendingSearchKeyWord = cityNameQuery
                        _commonError.value = Event(it.message.safe())
                    }

                    else -> {
                        _commonError.value = Event(it.message.safe())
                    }
                }
            }).addToDisposables()
    }

    override fun getCityNameHistory() {
        getCityNameHistoryUseCase.execute()
            .map {
                it.map { cage ->
                    cageDomainToUI.map(cage)
                }
            }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                _loading.postValue(Event(true))
            }
            .doFinally {
                _loading.postValue(Event(false))
            }
            .subscribe({
                _cageHistoryUI.value = it
            }, {

            }).addToDisposables()
    }
}