package uit.nguyenhung.autonomousforecast.app.feature.weather

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import dagger.hilt.android.AndroidEntryPoint
import uit.nguyenhung.autonomousforecast.R
import uit.nguyenhung.autonomousforecast.app.feature.cage.CageActivity
import uit.nguyenhung.autonomousforecast.app.feature.cage.CageActivity.Companion.CITY_NAME_KEY
import uit.nguyenhung.autonomousforecast.app.feature.cage.CageActivity.Companion.WEATHERS_KEY
import uit.nguyenhung.autonomousforecast.app.model.WeatherUI
import uit.nguyenhung.autonomousforecast.databinding.ActivityWeatherBinding

@AndroidEntryPoint
class WeatherActivity : AppCompatActivity() {

    private lateinit var viewModel: IWeatherViewModel

    private lateinit var adapter: WeatherAdapter

    private val binding: ActivityWeatherBinding by lazy {
        DataBindingUtil.setContentView(this, R.layout.activity_weather)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProvider(
            this,
            defaultViewModelProviderFactory
        ).get(WeatherViewModel::class.java)

        binding.initView()
        viewModel.initViewModel()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CageActivity.CAGE_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            data?.extras?.let { safeBundle ->
                safeBundle.getParcelableArrayList<WeatherUI>(WEATHERS_KEY)?.let {
                    viewModel.setWeatherUI(it)
                }

                safeBundle.getString(CITY_NAME_KEY)?.let {
                    viewModel.setCityName(it)
                }
            }
        }
    }

    private fun ActivityWeatherBinding.initView() {
        adapter = WeatherAdapter(rvWeather)

        tvSearch.setOnClickListener {
            CageActivity.startActivityForResult(this@WeatherActivity, tvSearch.text)
        }
    }

    private fun IWeatherViewModel.initViewModel() {
        weatherUI.observe(this@WeatherActivity, {
            adapter.submitList(it)
        })

        cityName.observe(this@WeatherActivity, {
            binding.tvSearch.text = it
        })
    }
}