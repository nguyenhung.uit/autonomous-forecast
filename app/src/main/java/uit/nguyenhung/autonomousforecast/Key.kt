package uit.nguyenhung.autonomousforecast

object Keys {

    init {
        System.loadLibrary("native-lib")
    }

    external fun getOpenCageKey(): String

    external fun getOpenWeatherAppId(): String
}