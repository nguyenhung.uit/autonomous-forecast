package uit.nguyenhung.autonomousforecast.shared

import java.text.SimpleDateFormat
import java.util.*

var DEFAULT_PATTERN = "EEE, dd MMM yyyy, HH:mm"

fun Date.format(pattern: String = DEFAULT_PATTERN, locale: Locale = Locale.getDefault()): String {
    val simpleDateFormat = SimpleDateFormat(pattern, locale)
    return simpleDateFormat.format(this)
}