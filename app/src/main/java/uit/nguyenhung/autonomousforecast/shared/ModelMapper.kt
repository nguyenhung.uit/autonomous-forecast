package uit.nguyenhung.autonomousforecast.shared

interface ModelMapper<From, To> {

    fun map(from: From): To
}