package uit.nguyenhung.autonomousforecast.shared.exception

const val NO_INTERNET_MESSAGE = "No internet connection, please retry again"
const val INPUT_EMPTY_MESSAGE = "city name is empty"

class NoConnectionException(msg: String = NO_INTERNET_MESSAGE) : RuntimeException(msg)

class InputEmptyException(msg: String = INPUT_EMPTY_MESSAGE): RuntimeException(msg)