package uit.nguyenhung.autonomousforecast.data.interceptor

import android.content.Context
import okhttp3.Interceptor
import okhttp3.Response
import uit.nguyenhung.autonomousforecast.shared.NetworkUtil
import uit.nguyenhung.autonomousforecast.shared.exception.NoConnectionException
import javax.inject.Inject

class NetworkInterceptor @Inject constructor(
    private val mContext: Context
) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        if (NetworkUtil.isNotConnected(mContext)) {
            throw NoConnectionException()
        }

        val builder = chain.request().newBuilder()
        return chain.proceed(builder.build())
    }
}