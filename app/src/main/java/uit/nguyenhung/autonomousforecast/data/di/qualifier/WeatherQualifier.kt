package uit.nguyenhung.autonomousforecast.data.di.qualifier

import javax.inject.Qualifier

@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class WeatherQualifier