package uit.nguyenhung.autonomousforecast.data.di.module

import android.content.Context
import com.facebook.stetho.okhttp3.StethoInterceptor
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.Cache
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import uit.nguyenhung.autonomousforecast.BuildConfig
import uit.nguyenhung.autonomousforecast.data.di.qualifier.CageQualifier
import uit.nguyenhung.autonomousforecast.data.di.qualifier.WeatherQualifier
import uit.nguyenhung.autonomousforecast.data.source.cage.remote.CageService
import uit.nguyenhung.autonomousforecast.data.source.weather.remote.WeatherService
import uit.nguyenhung.autonomousforecast.data.interceptor.CachedDataNetworkInterceptor
import uit.nguyenhung.autonomousforecast.data.interceptor.HttpErrorHandlerInterceptor
import uit.nguyenhung.autonomousforecast.data.interceptor.NetworkInterceptor
import uit.nguyenhung.autonomousforecast.data.interceptor.OfflineNetworkInterceptor
import java.io.File
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton

const val LOGGING_INTERCEPTOR_NAMED = "LOGGING_INTERCEPTOR_NAMED"
const val HTTP_ERROR_HANDLER_INTERCEPTOR_NAMED = "HTTP_ERROR_HANDLER_INTERCEPTOR_NAMED"
const val NETWORK_INTERCEPTOR_NAMED = "NETWORK_INTERCEPTOR_NAMED"
const val CACHED_DATA_NETWORK_INTERCEPTOR_NAMED = "CACHED_DATA_NETWORK_INTERCEPTOR_NAMED"
const val OFFLINE_NETWORK_INTERCEPTOR_NAMED = "OFFLINE_NETWORK_INTERCEPTOR_NAMED"

const val HEADER_CACHE_CONTROL = "Cache-Control"
const val HEADER_PRAGMA = "Pragma"
private const val cacheSize = (5 * 1024 * 1024).toLong() // 5 MB

const val DEFAULT_TIME_OUT_IN_SECONDS = 10L // in seconds

@Module
@InstallIn(SingletonComponent::class)
class NetworkModule {

    @Singleton
    @Provides
    @Named(LOGGING_INTERCEPTOR_NAMED)
    internal fun provideHttpLoggingInterceptor(): Interceptor = HttpLoggingInterceptor()
        .apply {
            level = HttpLoggingInterceptor.Level.BODY
        }


    @Singleton
    @Provides
    @Named(HTTP_ERROR_HANDLER_INTERCEPTOR_NAMED)
    internal fun provideHttpErrorHandlerInterceptor(): Interceptor =
        HttpErrorHandlerInterceptor()

    @Singleton
    @Provides
    @Named(NETWORK_INTERCEPTOR_NAMED)
    internal fun provideNetworkInterceptor(@ApplicationContext context: Context): Interceptor =
        NetworkInterceptor(context)

    @Singleton
    @Provides
    @Named(CACHED_DATA_NETWORK_INTERCEPTOR_NAMED)
    internal fun provideCachedDataNetworkInterceptor(): Interceptor =
        CachedDataNetworkInterceptor()

    @Singleton
    @Provides
    @Named(OFFLINE_NETWORK_INTERCEPTOR_NAMED)
    internal fun provideOfflineNetworkInterceptor(@ApplicationContext context: Context): Interceptor =
        OfflineNetworkInterceptor(context)

    @Singleton
    @Provides
    internal fun provideCached(@ApplicationContext context: Context) =
        Cache(File(context.cacheDir, "weather_cache_file"), cacheSize)

    @Singleton
    @Provides
    internal fun provideOkHttpClient(
        cache: Cache,
        @Named(LOGGING_INTERCEPTOR_NAMED) httpLoggingInterceptor: Interceptor,
        @Named(HTTP_ERROR_HANDLER_INTERCEPTOR_NAMED) httpErrorHandlerInterceptor: Interceptor,
        @Named(NETWORK_INTERCEPTOR_NAMED) networkInterceptor: Interceptor,
        @Named(CACHED_DATA_NETWORK_INTERCEPTOR_NAMED) cachedDataNetworkInterceptor: Interceptor,
        @Named(OFFLINE_NETWORK_INTERCEPTOR_NAMED) offlineNetworkInterceptor: Interceptor,
    ): OkHttpClient {
        val okBuilder = OkHttpClient.Builder()

        .cache(cache)
        .addNetworkInterceptor(cachedDataNetworkInterceptor)

        okBuilder.addInterceptor(networkInterceptor)

        if (BuildConfig.DEBUG) {
            okBuilder
                .addNetworkInterceptor(StethoInterceptor())
                .addInterceptor(httpLoggingInterceptor)
        }

        okBuilder
            .addInterceptor(httpErrorHandlerInterceptor)
            .connectTimeout(DEFAULT_TIME_OUT_IN_SECONDS, TimeUnit.SECONDS)
            .readTimeout(DEFAULT_TIME_OUT_IN_SECONDS, TimeUnit.SECONDS)
            .writeTimeout(DEFAULT_TIME_OUT_IN_SECONDS, TimeUnit.SECONDS)
        return okBuilder.build()
    }

    @Singleton
    @Provides
    @WeatherQualifier
    internal fun provideWeatherRetrofit(
        okHttpClient: OkHttpClient,
        gson: Gson
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BuildConfig.OPEN_WEATHER_URL)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create()).build()
    }

    @Singleton
    @Provides
    @CageQualifier
    internal fun provideCageRetrofit(
        okHttpClient: OkHttpClient,
        gson: Gson
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BuildConfig.OPEN_CAGE_URL)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create()).build()
    }

    @Provides
    @Singleton
    internal fun provideWeatherService(@WeatherQualifier retrofit: Retrofit) =
        retrofit.create(WeatherService::class.java)

    @Provides
    @Singleton
    internal fun provideCageService(@CageQualifier retrofit: Retrofit) =
        retrofit.create(CageService::class.java)
}