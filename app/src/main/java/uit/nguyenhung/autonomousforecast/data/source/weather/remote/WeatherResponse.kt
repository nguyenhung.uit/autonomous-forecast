package uit.nguyenhung.autonomousforecast.data.source.weather.remote

import com.google.gson.annotations.SerializedName

data class WeatherForecastResponse(
    @SerializedName("hourly") val weatherHourlyResponse: List<WeatherHourlyResponse>?
)

data class WeatherHourlyResponse(
    @SerializedName("dt") val dateTime: Long?,
    @SerializedName("temp") val temperature: Double?,
    @SerializedName("pressure") val pressure: Long?,
    @SerializedName("humidity") val humidity: Long?,
    @SerializedName("weather") val weathers: List<WeatherResponse>?
)

data class WeatherResponse(
    @SerializedName("description") val description: String?
)