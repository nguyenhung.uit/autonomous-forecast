package uit.nguyenhung.autonomousforecast.data.source.cage.remote

import io.reactivex.Single
import uit.nguyenhung.autonomousforecast.data.source.cage.local.SearchHistoryDao
import uit.nguyenhung.autonomousforecast.domain.mapper.CageResponseToDomain
import uit.nguyenhung.autonomousforecast.domain.mapper.SearchHistoryEntityToDomain
import uit.nguyenhung.autonomousforecast.domain.mapper.SearchHistoryResponseToEntity
import uit.nguyenhung.autonomousforecast.domain.model.Cage
import uit.nguyenhung.autonomousforecast.domain.repository.ICageRepository
import javax.inject.Inject

class CageRepository @Inject constructor(
    private val searchHistoryDao: SearchHistoryDao,
    private val cageService: CageService,
    private val cageResponseToDomain: CageResponseToDomain,
    private val searchHistoryEntityToDomain: SearchHistoryEntityToDomain,
    private val searchHistoryResponseToEntity: SearchHistoryResponseToEntity
) : ICageRepository {

    override fun getLatLongByCityName(cityNameQuery: String): Single<Cage> {
        return cageService.getLatLongByCityName(cityNameQuery)
            .doOnSuccess {
                it.results?.firstOrNull()?.let { safeCageResultResponse ->
                    val searchHistoryEntity =
                        searchHistoryResponseToEntity.map(safeCageResultResponse).apply {
                            cityName = cityNameQuery
                        }
                    searchHistoryDao.insertCage(searchHistoryEntity)
                }
            }
            .map {
                it.results?.firstOrNull()?.let { safeCageResultResponse ->
                    cageResponseToDomain.map(safeCageResultResponse)
                }
            }
    }

    override fun getCityNameHistory(): Single<List<Cage>> {
        return searchHistoryDao.getSearchHistory().map {
            it.map { entity ->
                searchHistoryEntityToDomain.map(entity)
            }.reversed()
        }
    }
}