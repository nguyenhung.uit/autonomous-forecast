package uit.nguyenhung.autonomousforecast.data.source.cage.remote

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query
import uit.nguyenhung.autonomousforecast.Keys

interface CageService {

    @GET("geocode/v1/json")
    fun getLatLongByCityName(
        @Query("q") cityNameQuery: String,
        @Query("key") key: String = Keys.getOpenCageKey(),
        @Query("pretty") pretty: Int = 1,
        @Query("no_annotations") noAnnotations: Int = 1,
        @Query("no_dedupe") noDedupe: Int = 1,
        @Query("no_record") noRecord: Int = 1,
        @Query("limit") limit: Int = 1
    ): Single<CageResponse>
}