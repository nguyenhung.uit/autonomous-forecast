package uit.nguyenhung.autonomousforecast.data.source.cage.local

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index

const val SEARCH_HISTORY_TABLE_NAME = "SEARCH_HISTORY"
const val CITY_NAME = "city_name"
const val LAT = "lat"
const val LNG = "lng"

@Entity(
    primaryKeys = [CITY_NAME],
    tableName = SEARCH_HISTORY_TABLE_NAME,
    indices = [Index(value = [CITY_NAME])]
)
data class SearchHistoryEntity(
    @ColumnInfo(name = CITY_NAME) var cityName: String = "",
    @ColumnInfo(name = LAT) val lat: Double?,
    @ColumnInfo(name = LNG) val lng: Double?
)