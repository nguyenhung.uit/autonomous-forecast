package uit.nguyenhung.autonomousforecast.data.source.cage.local

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(
    entities = [
        SearchHistoryEntity::class
    ],
    version = 1,
    exportSchema = true
)
abstract class AppDatabase : RoomDatabase() {

    companion object {
        const val APP_DATABASE_NAME = "autonomous_forecast_db"
    }

    abstract fun createSearchHistoryDao(): SearchHistoryDao
}