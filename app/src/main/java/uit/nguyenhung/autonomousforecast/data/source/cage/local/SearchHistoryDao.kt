package uit.nguyenhung.autonomousforecast.data.source.cage.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Single

@Dao
interface SearchHistoryDao {

    @Query(
        """
        SELECT * FROM $SEARCH_HISTORY_TABLE_NAME
        """
    )
    fun getSearchHistory(): Single<List<SearchHistoryEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCage(entity: SearchHistoryEntity)
}