package uit.nguyenhung.autonomousforecast.data.source.weather.remote

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query
import uit.nguyenhung.autonomousforecast.Keys

interface WeatherService {

    @GET("data/2.5/onecall")
    fun getWeatherForecast(
        @Query("lat") lat: Double,
        @Query("lon") lng: Double,
        @Query("exclude") exclude: String = "daily",
        @Query("appid") appId: String = Keys.getOpenWeatherAppId()
    ): Single<WeatherForecastResponse>
}