package uit.nguyenhung.autonomousforecast.data.interceptor

import okhttp3.CacheControl
import okhttp3.Interceptor
import okhttp3.Response
import uit.nguyenhung.autonomousforecast.data.di.module.HEADER_CACHE_CONTROL
import uit.nguyenhung.autonomousforecast.data.di.module.HEADER_PRAGMA
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class CachedDataNetworkInterceptor @Inject constructor() : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val response: Response = chain.proceed(chain.request())
        val cacheControl = CacheControl.Builder()
            .maxAge(10, TimeUnit.SECONDS)
            .build()

        return response.newBuilder()
            .removeHeader(HEADER_PRAGMA)
            .removeHeader(HEADER_CACHE_CONTROL)
            .header(HEADER_CACHE_CONTROL, cacheControl.toString())
            .build()
    }
}