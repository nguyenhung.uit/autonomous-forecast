package uit.nguyenhung.autonomousforecast.data.source.weather.remote

import io.reactivex.Single
import uit.nguyenhung.autonomousforecast.domain.mapper.WeatherResponseToDomain
import uit.nguyenhung.autonomousforecast.domain.model.Weather
import uit.nguyenhung.autonomousforecast.domain.repository.IWeatherRepository
import javax.inject.Inject

class WeatherRepository @Inject constructor(
    private val weatherService: WeatherService,
    private val weatherResponseToDomain: WeatherResponseToDomain
) : IWeatherRepository {

    override fun getWeatherForecast(
        lat: Double,
        lng: Double,
    ): Single<List<Weather>> {
        return weatherService.getWeatherForecast(
            lat = lat,
            lng = lng
        ).map { weatherForecastResponse ->
            weatherForecastResponse.weatherHourlyResponse?.map { weatherHourlyResponse ->
                weatherResponseToDomain.map(weatherHourlyResponse)
            }
        }
    }

}