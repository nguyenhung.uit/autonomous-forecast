package uit.nguyenhung.autonomousforecast.data.source.cage.remote

import com.google.gson.annotations.SerializedName

data class CageResponse(
    @SerializedName("results") val results: List<CageResultResponse>?,
    @SerializedName("status") val status: CageStatusResponse?
)

data class CageResultResponse(
    @SerializedName("geometry") val geometry: CageGeoResponse?
)

data class CageGeoResponse(
    @SerializedName("lat") val lat: Double?,
    @SerializedName("lng") val long: Double?
)

data class CageStatusResponse(
    @SerializedName("code") val code: Int?,
    @SerializedName("message") val message: String?
)