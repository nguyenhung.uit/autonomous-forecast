package uit.nguyenhung.autonomousforecast.data.di.module

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import uit.nguyenhung.autonomousforecast.data.source.cage.remote.CageRepository
import uit.nguyenhung.autonomousforecast.data.source.weather.remote.WeatherRepository
import uit.nguyenhung.autonomousforecast.domain.repository.ICageRepository
import uit.nguyenhung.autonomousforecast.domain.repository.IWeatherRepository
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {

    @Singleton
    @Binds
    internal abstract fun provideWeatherRepository(repository: WeatherRepository): IWeatherRepository

    @Singleton
    @Binds
    internal abstract fun provideCageRepository(repository: CageRepository): ICageRepository
}