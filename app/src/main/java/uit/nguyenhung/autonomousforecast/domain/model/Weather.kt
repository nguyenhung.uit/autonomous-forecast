package uit.nguyenhung.autonomousforecast.domain.model

import java.util.*

data class Weather(
    val date: Date?,
    val temperature: Double?,
    val pressure: Long?,
    val humidity: Long?,
    val description: String?,
)