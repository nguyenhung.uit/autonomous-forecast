package uit.nguyenhung.autonomousforecast.domain.mapper

import uit.nguyenhung.autonomousforecast.data.source.cage.local.SearchHistoryEntity
import uit.nguyenhung.autonomousforecast.domain.model.Cage
import uit.nguyenhung.autonomousforecast.shared.ModelMapper
import javax.inject.Inject

class SearchHistoryEntityToDomain @Inject constructor() : ModelMapper<SearchHistoryEntity, Cage> {

    override fun map(from: SearchHistoryEntity) = Cage(
        lat = from.lat,
        long = from.lng,
        cityName = from.cityName
    )
}