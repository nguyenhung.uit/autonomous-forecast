package uit.nguyenhung.autonomousforecast.domain.model

data class Cage(
    val lat: Double?,
    val long: Double?,
    var cityName: String = "",
)