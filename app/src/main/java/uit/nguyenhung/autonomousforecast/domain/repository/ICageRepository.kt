package uit.nguyenhung.autonomousforecast.domain.repository

import io.reactivex.Single
import uit.nguyenhung.autonomousforecast.domain.model.Cage

interface ICageRepository {

    fun getLatLongByCityName(cityNameQuery: String): Single<Cage>

    fun getCityNameHistory(): Single<List<Cage>>
}