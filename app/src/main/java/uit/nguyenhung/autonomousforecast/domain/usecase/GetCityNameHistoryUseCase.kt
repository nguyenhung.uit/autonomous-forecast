package uit.nguyenhung.autonomousforecast.domain.usecase

import io.reactivex.Single
import uit.nguyenhung.autonomousforecast.domain.model.Cage
import uit.nguyenhung.autonomousforecast.domain.repository.ICageRepository
import javax.inject.Inject

interface IGetCityNameHistoryUseCase {
    fun execute(): Single<List<Cage>>
}

class GetCityNameHistoryUseCase @Inject constructor(
    private val cageRepository: ICageRepository
) : IGetCityNameHistoryUseCase {

    override fun execute(): Single<List<Cage>> {
        return cageRepository.getCityNameHistory()
    }
}