package uit.nguyenhung.autonomousforecast.domain.mapper

import uit.nguyenhung.autonomousforecast.data.source.weather.remote.WeatherHourlyResponse
import uit.nguyenhung.autonomousforecast.domain.model.Weather
import uit.nguyenhung.autonomousforecast.shared.ModelMapper
import java.util.*
import javax.inject.Inject

class WeatherResponseToDomain @Inject constructor() : ModelMapper<WeatherHourlyResponse, Weather> {

    override fun map(from: WeatherHourlyResponse) = Weather(
        date = from.dateTime?.let { Date(it * 1000) },
        temperature = from.temperature,
        pressure = from.pressure,
        humidity = from.humidity,
        description = from.weathers?.map { it.description }
            ?.joinToString(separator = "-") { it.toString() }
    )
}