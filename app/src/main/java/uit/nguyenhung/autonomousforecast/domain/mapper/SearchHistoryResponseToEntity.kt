package uit.nguyenhung.autonomousforecast.domain.mapper

import uit.nguyenhung.autonomousforecast.data.source.cage.local.SearchHistoryEntity
import uit.nguyenhung.autonomousforecast.data.source.cage.remote.CageResultResponse
import uit.nguyenhung.autonomousforecast.shared.ModelMapper
import javax.inject.Inject

class SearchHistoryResponseToEntity @Inject constructor() :
    ModelMapper<CageResultResponse, SearchHistoryEntity> {

    override fun map(from: CageResultResponse) = SearchHistoryEntity(
        lat = from.geometry?.lat,
        lng = from.geometry?.long
    )
}