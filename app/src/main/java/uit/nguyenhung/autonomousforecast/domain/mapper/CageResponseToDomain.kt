package uit.nguyenhung.autonomousforecast.domain.mapper

import uit.nguyenhung.autonomousforecast.data.source.cage.remote.CageResultResponse
import uit.nguyenhung.autonomousforecast.domain.model.Cage
import uit.nguyenhung.autonomousforecast.shared.ModelMapper
import javax.inject.Inject

class CageResponseToDomain @Inject constructor() : ModelMapper<CageResultResponse, Cage> {

    override fun map(from: CageResultResponse) = Cage(
        lat = from.geometry?.lat,
        long = from.geometry?.long
    )
}