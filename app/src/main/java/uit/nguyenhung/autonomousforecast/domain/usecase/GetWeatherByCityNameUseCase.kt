package uit.nguyenhung.autonomousforecast.domain.usecase

import io.reactivex.Single
import uit.nguyenhung.autonomousforecast.domain.model.Weather
import uit.nguyenhung.autonomousforecast.domain.repository.ICageRepository
import uit.nguyenhung.autonomousforecast.domain.repository.IWeatherRepository
import uit.nguyenhung.autonomousforecast.shared.exception.InputEmptyException
import uit.nguyenhung.autonomousforecast.shared.safe
import javax.inject.Inject

interface IGetWeatherByCityNameUseCase {
    fun execute(
        cityNameQuery: String
    ): Single<List<Weather>>
}

class GetWeatherByCityNameUseCase @Inject constructor(
    private val cageRepository: ICageRepository,
    private val weatherRepository: IWeatherRepository
) : IGetWeatherByCityNameUseCase {

    override fun execute(cityNameQuery: String): Single<List<Weather>> {
        if (cityNameQuery.isEmpty()) {
            return Single.error(InputEmptyException())
        }

        return cageRepository.getLatLongByCityName(cityNameQuery)
            .flatMap {
                weatherRepository.getWeatherForecast(it.lat.safe(), it.long.safe())
            }
    }
}