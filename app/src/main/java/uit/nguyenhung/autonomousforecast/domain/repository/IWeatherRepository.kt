package uit.nguyenhung.autonomousforecast.domain.repository

import io.reactivex.Single
import uit.nguyenhung.autonomousforecast.domain.model.Weather

interface IWeatherRepository {

    fun getWeatherForecast(lat: Double, lng: Double): Single<List<Weather>>
}