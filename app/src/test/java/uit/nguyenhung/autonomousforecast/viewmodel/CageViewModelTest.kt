package uit.nguyenhung.autonomousforecast.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.nhaarman.mockitokotlin2.*
import io.reactivex.Single
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.internal.verification.Times
import org.powermock.core.classloader.annotations.PowerMockIgnore
import org.powermock.core.classloader.annotations.PrepareForTest
import org.powermock.modules.junit4.PowerMockRunner
import uit.nguyenhung.autonomousforecast.app.Event
import uit.nguyenhung.autonomousforecast.app.feature.cage.CageViewModel
import uit.nguyenhung.autonomousforecast.app.feature.cage.ICageViewModel
import uit.nguyenhung.autonomousforecast.app.mapper.CageDomainToUI
import uit.nguyenhung.autonomousforecast.app.mapper.WeatherDomainToUI
import uit.nguyenhung.autonomousforecast.app.model.WeatherUI
import uit.nguyenhung.autonomousforecast.domain.model.Weather
import uit.nguyenhung.autonomousforecast.domain.usecase.GetCityNameHistoryUseCase
import uit.nguyenhung.autonomousforecast.domain.usecase.GetWeatherByCityNameUseCase
import uit.nguyenhung.autonomousforecast.rule.RxImmediateSchedulerRule
import uit.nguyenhung.autonomousforecast.shared.exception.InputEmptyException
import java.util.*

@RunWith(PowerMockRunner::class)
@PrepareForTest(
    value = [
        GetWeatherByCityNameUseCase::class,
        GetCityNameHistoryUseCase::class
    ]
)
@PowerMockIgnore("javax.management.*")
class CageViewModelTest {

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    @Rule
    @JvmField
    val testSchedulerRule = RxImmediateSchedulerRule()

    @Mock
    private lateinit var getWeatherByCityNameUseCase: GetWeatherByCityNameUseCase

    @Mock
    private lateinit var getCityNameHistoryUseCase: GetCityNameHistoryUseCase

    private val cageDomainToUI = CageDomainToUI()
    private val weatherDomainToUI = WeatherDomainToUI()

    private lateinit var viewModel: ICageViewModel

    private var cityQueryValid = "hanoi"
    private var cityQueryInValid = ""

    private val commonErrorObserver: Observer<Event<String>> = mock()
    private val inputErrorObserver: Observer<Event<String>> = mock()
    private val weathersObserver: Observer<List<WeatherUI>> = mock()

    private var weather1 = Weather(
        date = Date(1611378000000),
        temperature = 15.0,
        pressure = 1024,
        humidity = 35,
        description = "scattered clouds"
    )

    private var weather2 = Weather(
        date = Date(1611381600000),
        temperature = 20.0,
        pressure = 1019,
        humidity = 36,
        description = "sky is clear"
    )

    private var weathers = listOf(weather1, weather2)

    private var weatherUI1 = WeatherUI(
        id = 1611378000000,
        date = "Sat, 23 Jan 2021, 12:00",
        temperature = "15.0",
        pressure = "1024",
        humidity = "35%",
        description = "scattered clouds"
    )

    private var weatherUI2 = WeatherUI(
        id = 1611381600000,
        date = "Sat, 23 Jan 2021, 13:00",
        temperature = "20.0",
        pressure = "1019",
        humidity = "36%",
        description = "sky is clear"
    )

    private var weatherUIs = listOf(weatherUI1, weatherUI2)

    @Before
    fun prepareTest() {
        viewModel = CageViewModel(
            getWeatherByCityNameUseCase,
            getCityNameHistoryUseCase,
            cageDomainToUI,
            weatherDomainToUI
        )
    }

    @Test
    fun `input invalid cityquery, expect call inputError livedata`() {
        // arrange
        whenever(
            getWeatherByCityNameUseCase.execute(
                cityQueryInValid
            )
        ).thenReturn(Single.error(InputEmptyException()))
        viewModel.inputError.observeForever(inputErrorObserver)
        viewModel.weatherUI.observeForever(weathersObserver)

        // action
        viewModel.getWeatherByCityName(cityQueryInValid)

        // assert
        verify(inputErrorObserver, Times(1)).onChanged(any())
        verify(weathersObserver, never()).onChanged(weatherUIs)
    }

    @Test
    fun `input valid cityquery, expect call weathers livedata`() {
        // arrange
        whenever(
            getWeatherByCityNameUseCase.execute(
                cityQueryInValid
            )
        ).thenReturn(Single.just(weathers))

        viewModel.weatherUI.observeForever(weathersObserver)

        // action
        viewModel.getWeatherByCityName(cityQueryInValid)

        // assert
        verify(weathersObserver, Times(1)).onChanged(weatherUIs)
    }
}