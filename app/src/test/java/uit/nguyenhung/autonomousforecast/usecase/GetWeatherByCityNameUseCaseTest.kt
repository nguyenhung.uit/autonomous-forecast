package uit.nguyenhung.autonomousforecast.usecase

import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import uit.nguyenhung.autonomousforecast.domain.model.Cage
import uit.nguyenhung.autonomousforecast.domain.model.Weather
import uit.nguyenhung.autonomousforecast.domain.repository.ICageRepository
import uit.nguyenhung.autonomousforecast.domain.repository.IWeatherRepository
import uit.nguyenhung.autonomousforecast.domain.usecase.GetWeatherByCityNameUseCase
import uit.nguyenhung.autonomousforecast.domain.usecase.IGetWeatherByCityNameUseCase
import uit.nguyenhung.autonomousforecast.shared.exception.InputEmptyException
import uit.nguyenhung.autonomousforecast.shared.safe
import java.util.*

@RunWith(MockitoJUnitRunner::class)
class GetWeatherByCityNameUseCaseTest {

    @Mock
    private lateinit var cageRepository: ICageRepository

    @Mock
    private lateinit var weatherRepository: IWeatherRepository

    private lateinit var useCaseTest: IGetWeatherByCityNameUseCase

    private var weather1 = Weather(
        date = Date(1610946000000),
        temperature = 15.0,
        pressure = 1024,
        humidity = 35,
        description = "scattered clouds"
    )

    private var weather2 = Weather(
        date = Date(1611032400000),
        temperature = 20.0,
        pressure = 1019,
        humidity = 36,
        description = "sky is clear"
    )

    private var weathers = listOf(weather1, weather2)

    private var cityQueryValid = "hanoi"
    private var cityQueryInValid = ""

    private var cage = Cage(
        lat = 1.0,
        long = 1.0,
        cityName = cityQueryValid
    )

    @Before
    fun prepareTest() {
        useCaseTest = GetWeatherByCityNameUseCase(cageRepository, weatherRepository)
    }

    @Test
    fun `input valid cityQuery, expect receive a correct list of weather`() {
        // arrange
        whenever(
            cageRepository.getLatLongByCityName(cityQueryValid)
        ).thenReturn(Single.just(cage))

        whenever(
            weatherRepository.getWeatherForecast(cage.lat.safe(), cage.long.safe())
        ).thenReturn(Single.just(weathers))

        // action
        val testObserver = useCaseTest.execute(cityQueryValid).test()

        // assert
        testObserver.assertComplete()
        testObserver.assertValue {
            it == weathers
        }
    }

    @Test
    fun `input invalid cityQuery, expect receive a InputEmptyException`() {
        // arrange

        // action
        val testObserver = useCaseTest.execute(cityQueryInValid).test()

        // assert
        testObserver.assertError {
            it is InputEmptyException
        }
    }
}