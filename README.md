# Architecture & Principles:
Architecture: follow Clean Architecture

Principles: SOLID

# Third parties & libraries:
- Network: Okhttp and retrofit
- Thread handler: Rxjava, RxAndroid
- SQLite for caching: RoomDB
- MVVM pattern for Presentation & App layer using: ViewModel and Livedata in Android Architecture Components
- Dependency inversion principle: Dagger2
- Testing:
    + Unittest: mockito, powermock, junit

# Folder structure:

#### Application:
- App: presents the `app layer` and `presentation layer`.The layers using MVVM as a pattern
- Domain: presents the `domain layer` which handles the business logic of the application in usecases. The usecases can have repositories interface implemented in the data layer.
- Data: presents `data layer`. The layer is responsible for fetching the data including local source, remote source (rest API).
- Shared: contains const variables, extensions, util functions for the whole application.

#### Testing:
***Unit Test:***
    + Usecase: test usecases
    + ViewModel: test viewmodels
    
***UI Test:***
    + not yet

# Steps in order to get the application run on local computer:
- Clone source from the repository.
- Make sync to get dependences, libraries
- Instant `NDK (Native Development Kit)`, `LLDB (Low Level Debugger)`, `CMake`
- Clean the project then making the build.

# Checklist of items has done:
* Provide the UX/UI searching/history search forecast screen: 
    * Caching search history in `RoomDB`
* Caching the result of the search to prevent requests to the server many times:
    * Handle cached in `okhttp`. Prevent call many times with the same headers. The internal of preventing is 10 seconds. It means does not call 2 APIs with in the same time within 10s.
* If the network is not available, the application will auto request the query right after the network back:
    * Using observable pattern and `ConnectivityManager` built in android sdk to handle
* Setup project able to build with 3 environments development, staging, production:
    * Config in `gradle` for 3 enviroments
* Encrypt your configuration to prevent getting the server configuration(like appId):
    + Store appId into `.cpp` file
    + Using `Cmake`

# Additional information:
* Handle Configuration Change of Screens
* Programming language: Kotlin
* Design `whole project`: Clean architechture, design `app layer`: MVVM
* Write UnitTests: Unit Test and UI Test
* Handle InputException, NetworkException...


